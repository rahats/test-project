package mint.publishing.apps.testproject.configuration.security

import mint.publishing.apps.testproject.service.AccountService
import org.springframework.security.core.authority.SimpleGrantedAuthority
import org.springframework.security.core.userdetails.User
import org.springframework.security.core.userdetails.UserDetails
import org.springframework.security.core.userdetails.UserDetailsService
import org.springframework.security.core.userdetails.UsernameNotFoundException
import org.springframework.stereotype.Service

@Service
class UserDetailsServiceImpl(
    private val accountService: AccountService
) : UserDetailsService {
    override fun loadUserByUsername(username: String?): UserDetails {
        val user = accountService.findByUsername(username = username ?: "")
        if (username.isNullOrEmpty()) {
            throw UsernameNotFoundException("Account is not found")
        }
        return User(
            user.username,
            user.password,
            setOf(SimpleGrantedAuthority(user.role?.name))
        )
    }
}