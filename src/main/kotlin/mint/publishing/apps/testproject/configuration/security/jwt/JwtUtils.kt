package mint.publishing.apps.testproject.configuration.security.jwt

import io.jsonwebtoken.Claims
import io.jsonwebtoken.Jwts
import io.jsonwebtoken.SignatureAlgorithm
import org.springframework.beans.factory.annotation.Value
import org.springframework.security.core.userdetails.UserDetails
import org.springframework.stereotype.Component
import java.util.*
import kotlin.collections.HashMap

@Component
class JwtUtils {

    @Value("\${jwt.signing.secret.key}")
    private lateinit var jwtSigningSecretKey: String

    fun extractUsername(token: String): String {
        return extractClaim(token, Claims::getSubject)
    }

    fun extractExpiration(token: String): Date {
        return extractClaim(token, Claims::getExpiration)
    }

    private fun <T> extractClaim(token: String, claimsResolver: (Claims) -> T): T {
        val claims = extractAllClaims(token)
        return claimsResolver.invoke(claims)
    }

    private fun extractAllClaims(token: String): Claims {
        return Jwts.parser().setSigningKey(jwtSigningSecretKey).parseClaimsJws(token).body
    }

    private fun isTokenExpired(token: String): Boolean {
        return extractExpiration(token).before(Date())
    }

    fun generateToken(userDetails: UserDetails): String {
        val claims: MutableMap<String, Any> = HashMap()
        return createToken(claims, userDetails)
    }

    private fun createToken(claims: MutableMap<String, Any>, userDetails: UserDetails): String {
        val calendar = Calendar.getInstance()
        calendar.add(Calendar.SECOND, 30)
        return Jwts.builder().setClaims(claims).setSubject(userDetails.username)
            .claim("authorities", userDetails.authorities).setIssuedAt(Date(System.currentTimeMillis()))
            .setExpiration(calendar.time).signWith(SignatureAlgorithm.HS256, jwtSigningSecretKey).compact()
    }

    fun isTokenValid(token: String, userDetails: UserDetails): Boolean {
        val username = extractUsername(token)
        return username == userDetails.username && !isTokenExpired(token = token)
    }
}