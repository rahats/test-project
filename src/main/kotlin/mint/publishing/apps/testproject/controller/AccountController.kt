package mint.publishing.apps.testproject.controller

import mint.publishing.apps.testproject.model.payload.request.AccountEditRequest
import mint.publishing.apps.testproject.model.payload.request.AccountRegistrationRequest
import mint.publishing.apps.testproject.service.AccountService
import org.springframework.http.HttpStatus
import org.springframework.http.MediaType
import org.springframework.http.ResponseEntity
import org.springframework.security.access.prepost.PreAuthorize
import org.springframework.web.bind.annotation.*
import javax.validation.Valid
import javax.validation.constraints.NotNull

@RestController
@RequestMapping(
    value = ["/api/account"],
    consumes = [MediaType.APPLICATION_JSON_VALUE],
    produces = [MediaType.APPLICATION_JSON_VALUE]
)
@PreAuthorize("hasRole('ROLE_ADMIN')")
class AccountController(private val service: AccountService) {

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    fun createAccount(@RequestBody @Valid request: AccountRegistrationRequest) {
        service.createAccount(request = request)
    }

    @PatchMapping
    @ResponseStatus(HttpStatus.ACCEPTED)
    fun editAccount(@RequestBody @Valid request: AccountEditRequest) {
        service.updateAccount(request = request)
    }

    @PutMapping(value = ["/lock/unlock"])
    fun lockAndUnlockAccount(@RequestBody @NotNull request: Map<String, Long>): ResponseEntity<HashMap<String, String>> {
        return ResponseEntity.ok(service.lockAndUnlockAccount(accountId = request["accountId"]!!))
    }
}