package mint.publishing.apps.testproject.controller

import mint.publishing.apps.testproject.model.payload.request.DepartmentModificationRequest
import mint.publishing.apps.testproject.model.payload.response.DepartmentResponse
import mint.publishing.apps.testproject.service.DepartmentService
import org.springframework.http.HttpStatus
import org.springframework.http.MediaType
import org.springframework.http.ResponseEntity
import org.springframework.security.access.prepost.PreAuthorize
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PatchMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.ResponseStatus
import org.springframework.web.bind.annotation.RestController
import javax.validation.Valid

@RestController
@RequestMapping(
    value = ["/api/department"],
    produces = [MediaType.APPLICATION_JSON_VALUE],
    consumes = [MediaType.APPLICATION_JSON_VALUE]
)
class DepartmentController(private val service: DepartmentService) {

    @PreAuthorize("hasRole('ROLE_ADMIN')")
    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    fun createDepartment(@RequestBody @Valid request: DepartmentModificationRequest) {
        service.createDepartment(request = request)
    }

    @PreAuthorize("hasRole('ROLE_ADMIN')")
    @PatchMapping(*["/{departmentId}"])
    @ResponseStatus(HttpStatus.ACCEPTED)
    fun editDepartment(
        @PathVariable("departmentId") departmentId: Long, @RequestBody @Valid request: DepartmentModificationRequest
    ) {
        service.editDepartment(departmentId = departmentId, request = request)
    }

    @PreAuthorize("hasAnyRole('ROLE_ADMIN' , 'ROLE_COMPANY_MEMBER')")
    @GetMapping(*["/all"])
    fun departmentsList(): ResponseEntity<List<DepartmentResponse>> {
        return ResponseEntity.ok(service.departmentsList())
    }
}