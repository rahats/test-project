package mint.publishing.apps.testproject.controller

import mint.publishing.apps.testproject.model.payload.request.EmployeeCreationRequest
import mint.publishing.apps.testproject.model.payload.response.EmployeeRecordResponse
import mint.publishing.apps.testproject.service.DepartmentService
import mint.publishing.apps.testproject.service.EmployeeService
import org.springframework.http.MediaType
import org.springframework.http.ResponseEntity
import org.springframework.security.access.prepost.PreAuthorize
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController
import javax.validation.Valid

@RestController
@RequestMapping(
    value = ["/api/employee"],
    produces = [MediaType.APPLICATION_JSON_VALUE],
    consumes = [MediaType.APPLICATION_JSON_VALUE]
)
class EmployeeController(
    private val service: EmployeeService
) {

    @PreAuthorize("hasRole('ROLE_ADMIN')")
    @PostMapping
    fun createEmployee(@RequestBody @Valid employeeCreationRequest: EmployeeCreationRequest) {
        service.createEmployee(employeeCreationRequest = employeeCreationRequest)
    }

    @PreAuthorize("hasRole('ROLE_ADMIN')")
    @GetMapping
    fun employeesList(): ResponseEntity<List<EmployeeRecordResponse>> {
        return ResponseEntity.ok(service.employeesList())
    }
}