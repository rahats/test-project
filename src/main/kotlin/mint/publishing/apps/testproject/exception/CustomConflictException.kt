package mint.publishing.apps.testproject.exception

class CustomConflictException(message: String?) : Exception(message)