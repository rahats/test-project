package mint.publishing.apps.testproject.exception

class CustomForbiddenException(message: String?) : Exception(message)