package mint.publishing.apps.testproject.exception

import java.lang.Exception

class CustomNotFoundException(message: String?) : Exception(message)