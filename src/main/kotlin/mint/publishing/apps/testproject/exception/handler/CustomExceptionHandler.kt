package mint.publishing.apps.testproject.exception.handler

import jakarta.servlet.http.HttpServletRequest
import mint.publishing.apps.testproject.exception.CustomConflictException
import mint.publishing.apps.testproject.exception.CustomNotFoundException
import mint.publishing.apps.testproject.model.payload.response.FailureResponse
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.MethodArgumentNotValidException
import org.springframework.web.bind.annotation.ControllerAdvice
import org.springframework.web.bind.annotation.ExceptionHandler
import java.time.LocalDateTime

@ControllerAdvice
class CustomExceptionHandler {

    @ExceptionHandler(CustomConflictException::class)
    fun handlerConflictException(
        httpServlet: HttpServletRequest, exception: CustomConflictException
    ): ResponseEntity<FailureResponse> {
        return ResponseEntity.ok(
            FailureResponse(
                timestamp = LocalDateTime.now(),
                path = httpServlet.requestURI,
                message = exception.message!!,
                status = HttpStatus.CONFLICT
            )
        )
    }

    @ExceptionHandler(CustomNotFoundException::class)
    fun handlerNotFoundException(
        httpServlet: HttpServletRequest, exception: CustomConflictException
    ): ResponseEntity<FailureResponse> {
        return ResponseEntity.ok(
            FailureResponse(
                timestamp = LocalDateTime.now(),
                path = httpServlet.requestURI,
                message = exception.message!!,
                status = HttpStatus.NOT_FOUND
            )
        )
    }

    @ExceptionHandler(MethodArgumentNotValidException::class)
    fun handlerValidationException(
        httpServlet: HttpServletRequest, exception: CustomConflictException
    ): ResponseEntity<FailureResponse> {
        return ResponseEntity.ok(
            FailureResponse(
                timestamp = LocalDateTime.now(),
                path = httpServlet.requestURI,
                message = exception.message!!,
                status = HttpStatus.BAD_REQUEST
            )
        )
    }
}