package mint.publishing.apps.testproject.model.constant

enum class PaymentTypeConstant {
    CARD_TO_CARD, WEB_MONEY, KIWI
}