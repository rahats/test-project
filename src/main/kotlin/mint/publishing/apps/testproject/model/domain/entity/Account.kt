package mint.publishing.apps.testproject.model.domain.entity

import jakarta.persistence.*
import org.hibernate.annotations.CreationTimestamp
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.security.crypto.password.PasswordEncoder
import java.time.LocalDateTime

@Entity
@Table(name = "accounts")
data class Account(
    @Id @GeneratedValue(strategy = GenerationType.IDENTITY) val id: Long? = null,

    @Column(name = "username", nullable = false) var username: String = "",

    @Column(name = "password", nullable = false) var password: String = "",

    @OneToOne(mappedBy = "headAccount")
    @Column(name = "department", nullable = false)
    var department: Department? = null,

    @Enumerated(EnumType.STRING)
    @Column(name = "role", nullable = false)
    var role: Role? = null,

    @Column(name = "email", nullable = false)
    var email: String = "",

    @CreationTimestamp
    @Column(
        name = "created_at",
        nullable = false,
        updatable = false
    )
    val createdAt: LocalDateTime = LocalDateTime.now(),

    @Column(name = "is_active")
    var isActive: Boolean = true,

    @OneToOne(mappedBy = "account")
    val employee: Employee? = null
) {
    @Autowired
    @Transient
    private lateinit var passwordEncoder: PasswordEncoder


    @PrePersist
    fun hashPassword() {
        this.password = passwordEncoder.encode(password)
    }
}