package mint.publishing.apps.testproject.model.domain.entity

import jakarta.persistence.*

@Entity
@Table(name = "departments")
data class Department(
    @Id @GeneratedValue(strategy = GenerationType.IDENTITY) val id: Long = 0,

    @OneToOne @JoinColumn(name = "head_account_id", nullable = false) var headAccount: Account = Account(),

    @Column(name = "code", nullable = false) var code: String = "",

    @Column(name = "name", nullable = false) var name: String = "",

    @OneToMany(mappedBy = "department") val employeeRecords: ArrayList<EmployeeRecord>? = null

)