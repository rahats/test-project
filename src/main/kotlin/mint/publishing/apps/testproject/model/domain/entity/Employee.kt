package mint.publishing.apps.testproject.model.domain.entity

import jakarta.persistence.*

@Entity
@Table(name = "employees")
data class Employee(
    @Id @GeneratedValue(strategy = GenerationType.IDENTITY) val id: Long = 0,

    @OneToOne @JoinColumn(name = "account_id", referencedColumnName = "id") val account: Account = Account(),

    @Column(name = "full_name", nullable = false) val fullName: String = "",

    @OneToMany(mappedBy = "employee")
    val employeeRecords: ArrayList<EmployeeRecord> = ArrayList()
)