package mint.publishing.apps.testproject.model.domain.entity

import jakarta.persistence.*

@Entity
@Table(name = "employee_records")
data class EmployeeRecord(
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    val id: Long? = null,

    @ManyToOne
    @JoinColumn(name = "employee_id", nullable = false)
    val employee: Employee = Employee(),

    @ManyToOne
    @JoinColumn(name = "department_id", nullable = false)
    val department: Department = Department(),

    @Column(name = "salary", nullable = false)
    val salary: Int = 0,

    @Column(name = "currency", nullable = false)
    val currency: String = "",


    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(
        name = "record_requisites",
        joinColumns = [JoinColumn(name = "record_id")],
        inverseJoinColumns = [JoinColumn(name = "requisite_id")]
    )
    val requisites: ArrayList<Requisite> = ArrayList()
)