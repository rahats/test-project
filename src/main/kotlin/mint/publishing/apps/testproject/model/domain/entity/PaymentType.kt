package mint.publishing.apps.testproject.model.domain.entity

import jakarta.persistence.*
import mint.publishing.apps.testproject.model.constant.PaymentTypeConstant

@Entity
@Table(name = "payment_types")
data class PaymentType(
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    val id: Long? = null,

    @Enumerated(EnumType.STRING)
    @Column(name = "name" , nullable = false)
    val name: PaymentTypeConstant? = null,

    @Column(name = "validation", nullable = false)
    val validation: String? = null,

    @OneToMany(mappedBy = "paymentType")
    val requisites: ArrayList<Requisite>? = null
)