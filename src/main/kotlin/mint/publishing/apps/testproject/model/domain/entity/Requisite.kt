package mint.publishing.apps.testproject.model.domain.entity

import jakarta.persistence.*

@Entity
@Table(name = "requisites")
data class Requisite(
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    val id: Long? = null,

    @ManyToOne
    @JoinColumn(name = "payment_type_id", nullable = false)
    val paymentType: PaymentType? = null,

    @Column(name = "value")
    val value: String? = null,

    @ManyToMany(mappedBy = "requisites")
    val employeeRecords: ArrayList<EmployeeRecord>? = null
)