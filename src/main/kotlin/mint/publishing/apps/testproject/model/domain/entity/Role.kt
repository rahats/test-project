package mint.publishing.apps.testproject.model.domain.entity

enum class Role {
    ROLE_ADMIN, ROLE_COMPANY_MEMBER, ROLE_HEAD_OF_DEPARTMENT
}