package mint.publishing.apps.testproject.model.domain.repository

import mint.publishing.apps.testproject.model.domain.entity.Account
import mint.publishing.apps.testproject.model.domain.entity.Department
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository

@Repository
interface DepartmentRepository : JpaRepository<Department, Long> {
    fun existsByNameOrHeadAccount(name: String, account: Account): Boolean
    fun existsAllByIdIn(ids: List<Long>) : Boolean
}