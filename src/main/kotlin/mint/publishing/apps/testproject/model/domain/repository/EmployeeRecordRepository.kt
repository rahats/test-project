package mint.publishing.apps.testproject.model.domain.repository

import mint.publishing.apps.testproject.model.domain.entity.EmployeeRecord
import mint.publishing.apps.testproject.model.domain.entity.Requisite
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository

@Repository
interface EmployeeRecordRepository : JpaRepository<EmployeeRecord, Long> {
    fun existsByEmployee_Account_UsernameAndDepartment_Id(username: String, departmentId: Long): Boolean
    fun existsByRequisites(requisites: List<Requisite>) : Boolean
}