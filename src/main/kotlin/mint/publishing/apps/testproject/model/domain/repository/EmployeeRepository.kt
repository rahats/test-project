package mint.publishing.apps.testproject.model.domain.repository

import mint.publishing.apps.testproject.model.domain.entity.Employee
import mint.publishing.apps.testproject.model.domain.entity.EmployeeRecord
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository

@Repository
interface EmployeeRepository : JpaRepository<Employee, Long> {
    fun existsByAccount_Username(username: String): Boolean
    fun existsByEmployeeRecords(employeeRecords: List<EmployeeRecord>) : Boolean
}