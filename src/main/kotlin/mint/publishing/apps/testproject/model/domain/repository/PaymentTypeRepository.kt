package mint.publishing.apps.testproject.model.domain.repository

import mint.publishing.apps.testproject.model.constant.PaymentTypeConstant
import mint.publishing.apps.testproject.model.domain.entity.PaymentType
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository

@Repository
interface PaymentTypeRepository : JpaRepository<PaymentType, Long> {
    fun findByPaymentType(paymentType: PaymentTypeConstant): PaymentType?
}