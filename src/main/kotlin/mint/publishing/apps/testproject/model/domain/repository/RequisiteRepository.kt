package mint.publishing.apps.testproject.model.domain.repository

import mint.publishing.apps.testproject.model.domain.entity.EmployeeRecord
import mint.publishing.apps.testproject.model.domain.entity.Requisite
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository

@Repository
interface RequisiteRepository : JpaRepository<Requisite, Long> {
    fun existsByEmployeeRecords(employeeRecords: List<EmployeeRecord>) : Boolean
}