package mint.publishing.apps.testproject.model.dto

data class NewEmployeeCredentials(
    var username: String,
    var password: String
)