package mint.publishing.apps.testproject.model.payload.request

import mint.publishing.apps.testproject.model.domain.entity.Role
import javax.validation.constraints.Email
import javax.validation.constraints.NotNull

data class AccountEditRequest(
    val id: Long,
    @NotNull(message = "Field \"Login\" is required") val username: String,
    @Email(message = "Field \"Email\" must match the e-mail pattern") @NotNull(message = "Field \"Email\" is required")
    val email: String,
    @NotNull(message = "Field \"Role\" is required") val role: Role
)