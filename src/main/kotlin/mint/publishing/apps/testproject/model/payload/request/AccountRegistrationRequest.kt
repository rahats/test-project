package mint.publishing.apps.testproject.model.payload.request

import mint.publishing.apps.testproject.model.domain.entity.Role
import javax.validation.constraints.NotNull

data class AccountRegistrationRequest(
    @NotNull(message = "Field \"Username\" is required") val username: String,
    @NotNull(message = "Field \"Email\" is required") val email: String,
    @NotNull(message = "Field \"Password\" is required") val password: String,
    @NotNull(message = "Field \"Role\" is required") val role: Role
)