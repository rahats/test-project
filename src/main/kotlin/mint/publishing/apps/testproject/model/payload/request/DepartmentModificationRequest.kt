package mint.publishing.apps.testproject.model.payload.request

import javax.validation.constraints.NotNull

data class DepartmentModificationRequest(
    val accountId: Long,
    @NotNull(message = "Field \"DepartmentName\" is required")
    val departmentName: String,
    @NotNull(message = "Field \"DepartmentCode\" is required")
    val departmentCode: String
)