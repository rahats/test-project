package mint.publishing.apps.testproject.model.payload.request

import mint.publishing.apps.testproject.model.constant.PaymentTypeConstant
import javax.validation.Valid
import javax.validation.constraints.NotEmpty
import javax.validation.constraints.NotNull
import javax.validation.constraints.Size

data class EmployeeCreationRequest(
    @Size(min = 2, message = "Field \"FullName\" must have minimum 2 words")
    @NotNull(message = "Field \"FullName\" is required")
    val fullName: String,
    @NotEmpty(message = "Field \"departmentsIds\" is required")
    val departmentIds: List<Long>,
    @NotNull(message = "Field \"Salary\" is required")
    val salary: Int, // Comment: Там написано целое число , я решил выбрать Int
    @NotNull(message = "Field \"Currency\" is required")
    val currency: String,
    @Valid
    @NotEmpty(message = "Field \"departmentsIds\" is required")
    val requisites: List<RequisiteRequest>
)

data class RequisiteRequest(
    @NotNull(message = "Field \"Requisite\" is required field") val requisite: String,
    @NotNull(message = "Field \"PaymentType\" is required field") val paymentType: PaymentTypeConstant
)