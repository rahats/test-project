package mint.publishing.apps.testproject.model.payload.response

data class DepartmentResponse(
    val id: Long,
    val name: String,
    val headUsername: String,
    val salarySum: Int
)