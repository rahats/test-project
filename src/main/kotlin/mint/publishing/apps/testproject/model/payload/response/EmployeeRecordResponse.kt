package mint.publishing.apps.testproject.model.payload.response

data class EmployeeRecordResponse(
    val id: Long,
    val fullName: String,
    val departments: List<EmployeeDepartmentResponse>? = null,
    val salary: Int,
    val currency: String
)

data class EmployeeDepartmentResponse(
    val code: String,
    val requisites: List<EmployeeDepartmentRequisiteResponse>
)

data class EmployeeDepartmentRequisiteResponse(
    val paymentType: String,
    val value: String
)