package mint.publishing.apps.testproject.model.payload.response

import org.springframework.http.HttpStatus
import java.time.LocalDateTime

data class FailureResponse(
    val timestamp: LocalDateTime,
    val path: String,
    val message: String,
    val status: HttpStatus
)