package mint.publishing.apps.testproject.service

import mint.publishing.apps.testproject.model.domain.entity.Account
import mint.publishing.apps.testproject.model.dto.NewEmployeeCredentials
import mint.publishing.apps.testproject.model.payload.request.AccountEditRequest
import mint.publishing.apps.testproject.model.payload.request.AccountRegistrationRequest

interface AccountService {
    fun createAccount(request: AccountRegistrationRequest)
    fun updateAccount(request: AccountEditRequest)
    fun lockAndUnlockAccount(accountId: Long): HashMap<String, String>
    fun findAccountById(accountId: Long): Account
    fun save(account: Account)
    fun generateRandomCredentialsForEmployee(fullName: String): NewEmployeeCredentials
    fun findByUsername(username: String): Account
}