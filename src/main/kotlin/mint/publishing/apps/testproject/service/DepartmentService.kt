package mint.publishing.apps.testproject.service

import mint.publishing.apps.testproject.model.domain.entity.Department
import mint.publishing.apps.testproject.model.payload.request.DepartmentModificationRequest
import mint.publishing.apps.testproject.model.payload.response.DepartmentResponse

interface DepartmentService {
    fun createDepartment(request: DepartmentModificationRequest)
    fun editDepartment(departmentId: Long, request: DepartmentModificationRequest)
    fun departmentsList(): List<DepartmentResponse>
    fun existsByIds(ids: List<Long>): Boolean
    fun findById(id: Long): Department
}