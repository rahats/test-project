package mint.publishing.apps.testproject.service

import mint.publishing.apps.testproject.model.domain.entity.EmployeeRecord
import mint.publishing.apps.testproject.model.domain.entity.Requisite
import mint.publishing.apps.testproject.model.payload.response.EmployeeRecordResponse

interface EmployeeRecordService {
    fun existByEmployeeAndDepartments(username: String, departmentId: Long): Boolean
    fun employeeRecordsList(): List<EmployeeRecordResponse>
    fun save(employeeRecord: EmployeeRecord)
    fun existsByRequisites(requisites: List<Requisite>): Boolean
}