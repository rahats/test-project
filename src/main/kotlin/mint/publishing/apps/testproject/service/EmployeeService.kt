package mint.publishing.apps.testproject.service

import mint.publishing.apps.testproject.model.payload.request.EmployeeCreationRequest
import mint.publishing.apps.testproject.model.payload.response.EmployeeRecordResponse

interface EmployeeService {
    fun createEmployee(employeeCreationRequest: EmployeeCreationRequest)
    fun employeesList(): List<EmployeeRecordResponse>
}