package mint.publishing.apps.testproject.service

import mint.publishing.apps.testproject.model.constant.PaymentTypeConstant
import mint.publishing.apps.testproject.model.domain.entity.PaymentType

interface PaymentTypeService {
    fun findByPaymentType(paymentType: PaymentTypeConstant) : PaymentType
}