package mint.publishing.apps.testproject.service

import mint.publishing.apps.testproject.model.domain.entity.EmployeeRecord
import mint.publishing.apps.testproject.model.domain.entity.Requisite

interface RequisiteService {
    fun existsByEmployeeRecords(employeeRecords: List<EmployeeRecord>): Boolean
    fun save(requisite: Requisite)
}