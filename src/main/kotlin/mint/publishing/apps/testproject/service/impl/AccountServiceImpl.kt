package mint.publishing.apps.testproject.service.impl

import com.fasterxml.jackson.databind.ObjectMapper
import mint.publishing.apps.testproject.exception.CustomConflictException
import mint.publishing.apps.testproject.exception.CustomNotFoundException
import mint.publishing.apps.testproject.model.domain.entity.Account
import mint.publishing.apps.testproject.model.domain.repository.AccountRepository
import mint.publishing.apps.testproject.model.dto.NewEmployeeCredentials
import mint.publishing.apps.testproject.model.payload.request.AccountEditRequest
import mint.publishing.apps.testproject.model.payload.request.AccountRegistrationRequest
import mint.publishing.apps.testproject.service.AccountService
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional
import kotlin.random.Random

@Service
@Transactional(readOnly = true)
class AccountServiceImpl(
    private val repository: AccountRepository,
    private val objectMapper: ObjectMapper
) : AccountService {

    @Transactional
    override fun createAccount(request: AccountRegistrationRequest) {
        if (repository.existsByUsername(request.username)) {
            throw CustomConflictException(
                message = String.format(
                    "Account with login : %s is already exists", request.username
                )
            )
        }
        if (repository.existsByEmail(request.email)) {
            throw CustomConflictException(
                message = String.format(
                    "Account with email : %s is already exists", request.email
                )
            )
        }
        save(
            Account(
                username = request.username, password = request.password, role = request.role, email = request.email
            )
        )
    }

    @Transactional
    override fun updateAccount(request: AccountEditRequest) {
        val account = findAccountById(accountId = request.id)
        if (!account.isActive) {
            throw CustomConflictException("Account is blocked")
        }
        if (repository.existsByUsername(request.username)) {
            throw CustomConflictException(
                String.format(
                    "Account with login : %s is already exists", request.username
                )
            )
        }
        if (repository.existsByEmail(request.email)) {
            throw CustomConflictException(
                String.format(
                    "Account with email : %s is already exists", request.email
                )
            )
        }
        account.username = request.username
        account.email = request.email
        account.role = request.role
    }

    @Transactional
    override fun lockAndUnlockAccount(accountId: Long): HashMap<String, String> {
        val account = findAccountById(accountId = accountId)
        account.isActive = !account.isActive
        val responsePayload = objectMapper.createObjectNode()
        return hashMapOf(
            "status" to (if (account.isActive) "ACTIVE" else "BLOCKED")
        )
    }

    override fun findAccountById(accountId: Long): Account {
        return repository.findById(accountId).orElseThrow { CustomNotFoundException("Account is not found") }
    }

    override fun save(account: Account) {
        repository.save(account)
    }

    override fun generateRandomCredentialsForEmployee(fullName: String): NewEmployeeCredentials {
        return NewEmployeeCredentials(username = generateUniqueLogin(fullName), password = "123456")
    }

    override fun findByUsername(username: String): Account {
        return repository.findByUsername(username = username)
    }

    private fun generateUniqueLogin(fullName: String): String {
        val username = fullName.split(" ")
            .map { it.first().lowercaseChar() }
            .joinToString("")

        var uniqueUsername = username
        var counter = Random.nextInt(1, 10)
        while (isUsernameTaken(uniqueUsername)) {
            uniqueUsername = "$username$counter"
            counter++
        }
        return uniqueUsername
    }

    private fun isUsernameTaken(username: String): Boolean {
        return repository.existsByUsername(username = username)
    }
}