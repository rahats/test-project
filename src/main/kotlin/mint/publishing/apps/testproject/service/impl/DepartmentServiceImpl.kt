package mint.publishing.apps.testproject.service.impl

import mint.publishing.apps.testproject.exception.CustomConflictException
import mint.publishing.apps.testproject.exception.CustomNotFoundException
import mint.publishing.apps.testproject.model.domain.entity.Department
import mint.publishing.apps.testproject.model.domain.repository.DepartmentRepository
import mint.publishing.apps.testproject.model.payload.request.DepartmentModificationRequest
import mint.publishing.apps.testproject.model.payload.response.DepartmentResponse
import mint.publishing.apps.testproject.service.AccountService
import mint.publishing.apps.testproject.service.DepartmentService
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional

@Service
@Transactional(readOnly = true)
class DepartmentServiceImpl(
    private val repository: DepartmentRepository,
    private val accountService: AccountService
) : DepartmentService {

    @Transactional
    override fun createDepartment(request: DepartmentModificationRequest) {
        val account = accountService.findAccountById(request.accountId)
        if (!account.isActive) {
            throw CustomConflictException("This account is locked")
        }
        if (repository.existsByNameOrHeadAccount(request.departmentName, account)) {
            throw CustomConflictException(
                String.format(
                    "Department with name : %s and head : %s is already exists", request.departmentCode,
                    account.username
                )
            )
        }
        val department = Department(
            name = request.departmentName, code = request.departmentCode, headAccount = account
        )
        repository.save(department)
        account.department = department
//        accountService.save(account = account)
    }

    @Transactional
    override fun editDepartment(departmentId: Long, request: DepartmentModificationRequest) {
        val department =
            repository.findById(departmentId).orElseThrow { CustomNotFoundException("Department does not exist") }
        val account = accountService.findAccountById(accountId = request.accountId)
        if (!account.isActive) {
            throw CustomConflictException("This account is locked")
        }
        if (repository.existsByNameOrHeadAccount(request.departmentName, account)) {
            throw CustomConflictException(
                String.format(
                    "Department with name : %s is already exists", request.departmentCode
                )
            )
        }
        department.name = request.departmentName
        department.code = request.departmentCode
        department.headAccount = account
        account.department = department
//        accountService.save(account = account)
    }

    override fun departmentsList(): List<DepartmentResponse> {
        val departments = repository.findAll()
        return departments.map { department ->
            DepartmentResponse(
                id = department.id,
                name = department.name,
                headUsername = department.headAccount.username,
                salarySum = department.employeeRecords?.sumOf {
                    it.salary ?: 0
                } ?: 0
            )
        }
    }

    override fun existsByIds(ids: List<Long>): Boolean {
        return repository.existsAllByIdIn(ids = ids)
    }

    override fun findById(id: Long): Department {
        return repository.findById(id).orElseThrow {
            CustomNotFoundException(
                String.format("Department with id : %s not found", id)
            )
        }
    }
}