package mint.publishing.apps.testproject.service.impl

import mint.publishing.apps.testproject.model.domain.entity.EmployeeRecord
import mint.publishing.apps.testproject.model.domain.entity.Requisite
import mint.publishing.apps.testproject.model.domain.repository.EmployeeRecordRepository
import mint.publishing.apps.testproject.model.payload.response.EmployeeDepartmentRequisiteResponse
import mint.publishing.apps.testproject.model.payload.response.EmployeeDepartmentResponse
import mint.publishing.apps.testproject.model.payload.response.EmployeeRecordResponse
import mint.publishing.apps.testproject.service.EmployeeRecordService
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional

@Service
@Transactional(readOnly = true)
class EmployeeRecordServiceImpl(private val repository: EmployeeRecordRepository) : EmployeeRecordService {

    override fun existByEmployeeAndDepartments(username: String, departmentId: Long): Boolean {
        return repository.existsByEmployee_Account_UsernameAndDepartment_Id(
            username = username, departmentId = departmentId
        )
    }

    override fun employeeRecordsList(): List<EmployeeRecordResponse> {
        val employeeRecords = repository.findAll()

        return employeeRecords.map { employeeRecord ->
            val employeeDepartmentRequisites = employeeRecord.requisites.map { requisite ->
                EmployeeDepartmentRequisiteResponse(
                    paymentType = requisite.paymentType?.name!!.name, value = requisite.value.orEmpty()
                )
            }

            val employeeDepartments = listOf(
                EmployeeDepartmentResponse(
                    code = employeeRecord.department.code, requisites = employeeDepartmentRequisites
                )
            )

            EmployeeRecordResponse(
                id = employeeRecord.employee.id,
                fullName = employeeRecord.employee.fullName,
                departments = employeeDepartments,
                salary = employeeRecord.salary,
                currency = employeeRecord.currency
            )
        }
    }

    override fun save(employeeRecord: EmployeeRecord) {
        repository.save(employeeRecord)
    }

    override fun existsByRequisites(requisites: List<Requisite>): Boolean {
        return repository.existsByRequisites(requisites = requisites)
    }
}