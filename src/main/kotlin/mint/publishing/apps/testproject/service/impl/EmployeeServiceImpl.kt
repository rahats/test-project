package mint.publishing.apps.testproject.service.impl

import mint.publishing.apps.testproject.exception.CustomConflictException
import mint.publishing.apps.testproject.model.domain.entity.*
import mint.publishing.apps.testproject.model.domain.repository.EmployeeRepository
import mint.publishing.apps.testproject.model.dto.NewEmployeeCredentials
import mint.publishing.apps.testproject.model.payload.request.EmployeeCreationRequest
import mint.publishing.apps.testproject.model.payload.request.RequisiteRequest
import mint.publishing.apps.testproject.model.payload.response.EmployeeRecordResponse
import mint.publishing.apps.testproject.service.*
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional

@Service
@Transactional(readOnly = true)
class EmployeeServiceImpl(
    private val repository: EmployeeRepository,
    private val accountService: AccountService,
    private val departmentService: DepartmentService,
    private val employeeRecordService: EmployeeRecordService,
    private val requisiteService: RequisiteService,
    private val paymentTypeService: PaymentTypeService
) : EmployeeService {

    @Transactional
    override fun createEmployee(employeeCreationRequest: EmployeeCreationRequest) {
        val departmentIds = employeeCreationRequest.departmentIds
        val fullName = employeeCreationRequest.fullName

        if (!departmentService.existsByIds(ids = departmentIds)) {
            throw CustomConflictException("Departments are not found")
        }

        val randomCredentials = generateUniqueCredentials(fullName)

        departmentIds.forEach { departmentId ->
            if (employeeRecordService.existByEmployeeAndDepartments(
                    username = randomCredentials.username,
                    departmentId = departmentId
                )
            ) {
                throw CustomConflictException("Employee already exists in these departments")
            }
        }

        val requisites = employeeCreationRequest.requisites.map { requisite ->
            validateAndSaveRequisite(requisite)
        }

        if (employeeRecordService.existsByRequisites(requisites = requisites)) {
            throw CustomConflictException("Employee with these requisites for these departments already exists")
        }

        val employeeRecords = departmentIds.map { departmentId ->
            val department = departmentService.findById(departmentId)
            EmployeeRecord(
                department = department,
                salary = employeeCreationRequest.salary,
                currency = employeeCreationRequest.currency,
                requisites = requisites as ArrayList<Requisite>
            ).also { employeeRecordService.save(it) }
        }

        val account = Account(
            username = randomCredentials.username,
            password = randomCredentials.password,
            role = Role.ROLE_COMPANY_MEMBER
        )
        accountService.save(account)

        if (repository.existsByEmployeeRecords(employeeRecords = employeeRecords)) {
            throw CustomConflictException("These requisites for records already exist")
        }

        val employee = Employee(
            account = account,
            fullName = fullName,
            employeeRecords = employeeRecords as ArrayList<EmployeeRecord>
        )
        repository.save(employee)
    }

    private fun generateUniqueCredentials(fullName: String): NewEmployeeCredentials {
        var randomCredentials: NewEmployeeCredentials
        do {
            randomCredentials = accountService.generateRandomCredentialsForEmployee(fullName)
        } while (repository.existsByAccount_Username(randomCredentials.username))
        return randomCredentials
    }

    private fun validateAndSaveRequisite(requisite: RequisiteRequest): Requisite {
        val paymentType = paymentTypeService.findByPaymentType(paymentType = requisite.paymentType)
        if (paymentType.validation?.toRegex()?.matches(requisite.requisite) == false) {
            throw CustomConflictException("Incorrect requisite for paymentType: ${requisite.paymentType}")
        }
        val requisiteEntity = Requisite(paymentType = paymentType, value = requisite.requisite)
        requisiteService.save(requisiteEntity)
        return requisiteEntity
    }


    override fun employeesList(): List<EmployeeRecordResponse> {
        return employeeRecordService.employeeRecordsList()
    }
}