package mint.publishing.apps.testproject.service.impl

import mint.publishing.apps.testproject.exception.CustomNotFoundException
import mint.publishing.apps.testproject.model.constant.PaymentTypeConstant
import mint.publishing.apps.testproject.model.domain.entity.PaymentType
import mint.publishing.apps.testproject.model.domain.repository.PaymentTypeRepository
import mint.publishing.apps.testproject.service.PaymentTypeService
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional

@Service
@Transactional(readOnly = true)
class PaymentTypeServiceImpl(private val repository: PaymentTypeRepository) : PaymentTypeService {
    override fun findByPaymentType(paymentType: PaymentTypeConstant): PaymentType {
        return repository.findByPaymentType(paymentType) ?: throw CustomNotFoundException("Payment type is not found")
    }
}