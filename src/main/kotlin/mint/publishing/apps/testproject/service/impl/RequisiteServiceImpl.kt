package mint.publishing.apps.testproject.service.impl

import mint.publishing.apps.testproject.model.domain.entity.EmployeeRecord
import mint.publishing.apps.testproject.model.domain.entity.Requisite
import mint.publishing.apps.testproject.model.domain.repository.RequisiteRepository
import mint.publishing.apps.testproject.service.RequisiteService
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional

@Service
@Transactional(readOnly = true)
class RequisiteServiceImpl(private val repository: RequisiteRepository) : RequisiteService {
    override fun existsByEmployeeRecords(employeeRecords: List<EmployeeRecord>): Boolean {
        return repository.existsByEmployeeRecords(employeeRecords = employeeRecords)
    }

    @Transactional
    override fun save(requisite: Requisite) {
        repository.save(requisite)
    }
}