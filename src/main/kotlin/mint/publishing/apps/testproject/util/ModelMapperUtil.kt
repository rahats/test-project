package mint.publishing.apps.testproject.util

import org.modelmapper.ModelMapper
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Component

@Component
class ModelMapperUtil private constructor() {
    private var modelMapper: ModelMapper? = null

    @Autowired
    constructor(modelMapper: ModelMapper?) : this() {
        this.modelMapper = modelMapper
    }

    fun <T> convertToDto(obj: Any?, tClass: Class<T>?): T {
        return modelMapper!!.map(obj, tClass)
    }
}